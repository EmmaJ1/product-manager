import React, {useState, useEffect} from 'react';

//props: {addProduct} {categories}
function ProductForm(props) {

    const [product, setProduct] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    const [articleNr, setArticleNr] = useState("");
    const [imageURL, setImageURL] = useState("");
    const [categoryList, setCategoryList] = useState([]);
    const [selectedCategory, setSelectedCategory] = useState("");
   
    useEffect(() => {
        setTimeout(() => {
            const categoryList = [
                {name: "Stavar", value:"Stavar"},
                {name: "Skidor", value: "Skidor"},
                {name: "Vantar", value: "Vantar"},
                {name: "Mössor", value: "Mössor"},
            ];
            setCategoryList(categoryList);
        }, 2000);
    }, []);
    
    return (
        <div className="container"style={{border:"1px solid black"}}>
            <h5> Product information</h5>
            
            <div className="container">
                <div className="row">
                <div className="col-6" style={{border:"1px solid black"}}>
                <div className="form-group">
                    <label htmlFor="product"> Product</label>
                    <input type="text" id="product" placeholder="Enter product name"
                            value={product}
                            onChange={event => {
                                setProduct(event.target.value);
                            }} />    
                </div> 
                <div className="form-group">
                    <label htmlFor="description"> Description </label>
                    <input type="text-area" id="description"
                        value={description}
                        onChange={event => {
                            setDescription(event.target.value);
                        }}
                    />    
                </div>
                <div className="form-group">
                    <label htmlFor="price"> Price</label>
                    <input type="text" id="price" placeholder="Enter price"
                        value={price}
                        onChange={event => {
                            setPrice(event.target.value);
                        }}
                    />    
                </div>
                <div className="form-group">
                    <label htmlFor="articleNr"> Article number</label>
                    <input type="text" id="articleNr" placeholder="Enter article number"
                        value={articleNr}
                        onChange={event => {
                            setArticleNr(event.target.value);
                        }}
                    />    
                </div>
                <div className="form-group">
                    <label htmlFor="imageURL"> Image</label>
                    <input type="text" id="imageURL" placeholder="Enter URL"
                        value={imageURL}
                        onChange={event => {
                            setImageURL(event.target.value);
                        }}
                    />    
                </div>
                <div className="form-group">
                    <label htmlFor="category"> Category</label>
                    <select id="inputstate" className="form-control"
                            value={selectedCategory}
                            onChange={event => {
                                setSelectedCategory(event.target.value);
                            }} >
                  
                       <option defaultValue> Select category </option>
                        {categoryList.map(category => (
                            <option value={category.value} > {category.name} </option>
                        ))}

                    </select>   
                </div>
                </div>
                
                    <div className="col-6" style={{border:"1px solid black"}}>
                        <h4> Image preview</h4>
                        <div className="container" style={{border:"1px solid black"}}>
                            <img src={imageURL} className="rounded mx-auto d-block" alt="..."></img>
                        </div>

                        <button 
                            onClick={() => {
                                const newProduct = {
                                    product, 
                                    description,
                                    price,
                                    articleNr,
                                    imageURL,
                                    category: selectedCategory
                                };
                                props.addProduct(newProduct);
                            }} >
                            Add product
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProductForm;