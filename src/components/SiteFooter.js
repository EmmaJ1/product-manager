import React from 'react';

function SiteHeader() {
    return (
        <header>
            <div className="container text-center" style={{border:"1px solid black", margin: "auto", padding:"10px"}}>
                <h6 style={{ padding: "10px"}}> Copyright 2019 Olivander´s </h6>
            </div>
        </header>
    )
}

export default SiteHeader;