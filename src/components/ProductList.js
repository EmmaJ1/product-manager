import React from 'react';

//props: productList
function ProductList(props) {
    return(
        <div className="container" style={{border:"1px solid black"}}>
            <h5>Product List </h5> 

            <table className="table" >
                <tbody>
                    <tr>
                        <th>Article nr.</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Category</th>
                    </tr>

                    {props.productList.map(product => (
                    <tr>
                        <td> {product.articleNr} </td>
                        <td> {product.product} </td>
                        <td> {product.price}   </td>
                        <td> {product.category} </td>
                    </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default ProductList;