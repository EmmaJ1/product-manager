import React, {useState} from 'react';
// import logo from './logo.svg';
import './App.css';

import SiteHeader from './components/SiteHeader';
import ProductForm from './components/ProductForm';
import ProductList from './components/ProductList';
import SiteFooter from './components/SiteFooter';


// const categories = [
//       {id: 1, name: "Tröjor", value:"trojor"},
//       {id: 2, name: "Jackor", value: "jackor"},
//       {id: 3, name: "Byxor", value: "byxor"},
//       {id: 4, name: "Mössor", value: "mossor"},
// ]

function App() {

  const [productList, setProductList] = useState([
    {
      articleNr: "1111",
      product: "Skin-skida",
      price: "1500",
      category: "Skidor"
    }
  ]);

  function addProduct(newProduct) {
    setProductList([...productList, newProduct]);
  }

  return (
    <div >
      <SiteHeader />

      {/* categories={categories} */}
      <ProductForm addProduct={addProduct} />  

      <ProductList productList={productList} />

      <SiteFooter />  
    </div>
  );
}

export default App;
